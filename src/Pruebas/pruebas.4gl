SCHEMA modula

MAIN
DEFINE fac RECORD LIKE facturafel_e.*
DEFINE comando STRING
DEFINE numero STRING
    CONNECT TO 'modula' AS 'informix' USING 'losfitos'
    DECLARE cur_fact CURSOR FOR
    SELECT *
      FROM facturafel_e
     ORDER BY fac_id
    --WHERE estatus = 'P'
    FOREACH cur_fact INTO fac.*
        CASE
            WHEN fac.fac_id < 10      LET numero = fac.fac_id USING "#"
            WHEN fac.fac_id < 100     LET numero = fac.fac_id USING "##"
            WHEN fac.fac_id < 1000    LET numero = fac.fac_id USING "###"
            WHEN fac.fac_id < 10000   LET numero = fac.fac_id USING "####"
            WHEN fac.fac_id < 100000  LET numero = fac.fac_id USING "#####"
            WHEN fac.fac_id < 1000000 LET numero = fac.fac_id USING "######"
        END CASE
        DISPLAY 'SOLICITA EJECUTAR PARA EL FAC_ID: ', numero
        DISPLAY CURRENT HOUR TO FRACTION(3)
        LET comando = 'fglrun ande_fel_g4s.42r ', numero CLIPPED, ' modula 0 0'
        RUN comando
        DISPLAY CURRENT HOUR TO FRACTION(3)
    END FOREACH
END MAIN