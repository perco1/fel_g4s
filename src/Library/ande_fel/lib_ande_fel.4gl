IMPORT security
IMPORT os
IMPORT XML

SCHEMA modula

FUNCTION ultimo_corr_msg()
DEFINE numero  LIKE factura_log.correlativo

     CALL STARTLOG("ERR_ANDE")
     
   SELECT MAX(correlativo) INTO numero
      FROM factura_log

   IF numero IS NULL THEN
      LET numero = 1
   ELSE
      LET numero = numero + 1
   END IF
RETURN numero
END FUNCTION

FUNCTION utl_nit_singuion(nit)
DEFINE resultado  SMALLINT
DEFINE nit        CHAR(25)
DEFINE caracter   CHAR(1)
DEFINE nit2       CHAR(25)
DEFINE i,j,k      SMALLINT
DEFINE r, n, suma SMALLINT
DEFINE rs1, rs2, rs3   SMALLINT
DEFINE digito     SMALLINT
DEFINE largo      SMALLINT

   LET nit = nit CLIPPED
   LET largo = LENGTH(nit)
   LET resultado = 1
   LET j = 0
   FOR i = 1 TO largo
      LET caracter = nit[i,i]
      IF caracter MATCHES "[0123456789KCF]" THEN
         LET j = j + 1
         LET nit2[j,j]=caracter
      ELSE
         IF caracter <> "-" OR caracter <> "." OR caracter <> " " THEN
            LET resultado = 0
            EXIT FOR
         END IF
      END IF 
   END FOR

   IF resultado = 1 THEN
      LET suma = 0
      FOR i = 1 TO (j - 1)
         LET k = j - (i -1)
         LET n = nit2[i,i]
         LET r = n * k
         LET suma = suma + r
      END FOR
      LET rs1 = suma MOD 11
      LET rs2 = rs1  MOD 11
      IF rs2 > 0 THEN
         LET rs3 = 11 - rs2
      ELSE
         LET rs3 = 0
      END IF
      IF nit2[j,j] = "K" THEN
         LET digito = 10
      ELSE
         LET digito = nit2[j,j]
      END IF
      IF digito = rs3 THEN
         LET resultado = 1
      ELSE
         LET resultado = 0
      END IF
   END IF

RETURN nit2
END FUNCTION

FUNCTION fel_archivo_xml(archivo)
DEFINE archivo VARCHAR(200)
DEFINE facxml  RECORD LIKE factura_xml.*
DEFINE maxi    INTEGER

     CALL STARTLOG("ERR_ANDE")

    IF archivo IS NOT NULL THEN
        LET maxi = 0
        
        SELECT MAX(cod_archivo_cer) INTO maxi
          FROM factura_xml

        IF maxi IS NULL THEN LET maxi = 0 END IF
        LET facxml.cod_archivo_cer = maxi + 1

        LOCATE facxml.archivo_xml IN MEMORY
        CALL facxml.archivo_xml.readFile(archivo)

        LET facxml.fecha_hora = CURRENT YEAR TO FRACTION(3)

        INSERT INTO factura_xml VALUES (facxml.*)

        FREE facxml.archivo_xml
    END IF
    
RETURN facxml.cod_archivo_cer
END FUNCTION

FUNCTION fel_archivo_genera()
DEFINE nombre STRING
    LET nombre = ult_archivo(security.RandomGenerator.CreateRandomString(10))

RETURN nombre
END FUNCTION

FUNCTION fel_archivo_elimina(nombre)
DEFINE nombre STRING
DEFINE resultado INTEGER

    LET resultado = os.Path.DELETE(nombre)

END FUNCTION

FUNCTION ult_archivo(arch)
DEFINE resultado  SMALLINT
DEFINE arch        CHAR(10)
DEFINE caracter   CHAR(1)
DEFINE arch2       CHAR(14)
DEFINE i,j  SMALLINT
DEFINE largo      SMALLINT

   LET arch = arch CLIPPED
   LET largo = LENGTH(arch)
   LET resultado = 1
   LET j = 0
   FOR i = 1 TO largo
      LET caracter = arch[i,i]
      IF caracter MATCHES "[a-z]" OR caracter MATCHES "[A-Z]" THEN
         LET j = j + 1
         LET arch2[j,j]=caracter
      END IF 
   END FOR

   LET arch2 = arch2 CLIPPED, ".xml"

RETURN arch2
END FUNCTION


FUNCTION fel_valida_directorio(fecha)
DEFINE fecha  DATE
DEFINE anio   CHAR(4)
DEFINE mes    CHAR(2)
DEFINE existe SMALLINT
DEFINE crea   SMALLINT
DEFINE dir2   STRING
DEFINE directorio STRING

     CALL STARTLOG("ERR_ANDE")
   
    LET anio = YEAR(fecha) USING "&&&&"
    LET mes  = MONTH(fecha) USING "&&"

    LET directorio = "../fel/",anio

    LET existe = os.Path.exists(directorio)

    IF NOT existe THEN
        LET crea = os.Path.mkDir(directorio)
        IF crea THEN
            LET existe = TRUE
        END IF
    END IF

    IF existe THEN
        LET directorio = directorio CLIPPED,"/",mes
        LET existe = os.Path.exists(directorio)
        IF NOT existe THEN
            LET crea = os.Path.mkDir(directorio)
        END IF
    END IF
    
RETURN directorio
END FUNCTION


FUNCTION fel_valida_directorio2(fecha)
DEFINE fecha  DATE
DEFINE anio   CHAR(4)
DEFINE mes    CHAR(2)
DEFINE existe SMALLINT
DEFINE crea   SMALLINT
DEFINE dir2   STRING
DEFINE directorio STRING

     CALL STARTLOG("ERR_ANDE")

    LET directorio = "../fel/temp/"
    
    LET existe = os.Path.exists(directorio)

    IF NOT existe THEN
        LET crea = os.Path.mkDir(directorio)
        IF crea THEN
            LET existe = TRUE
        END IF
    END IF
     
    LET anio = YEAR(fecha) USING "&&&&"
    LET mes  = MONTH(fecha) USING "&&"

    LET directorio = directorio CLIPPED,anio

    LET existe = os.Path.exists(directorio)

    IF NOT existe THEN
        LET crea = os.Path.mkDir(directorio)
        IF crea THEN
            LET existe = TRUE
        END IF
    END IF

    IF existe THEN
        LET directorio = directorio CLIPPED,"/",mes
        LET existe = os.Path.exists(directorio)
        IF NOT existe THEN
            LET crea = os.Path.mkDir(directorio)
            IF crea THEN
                LET dir2 = directorio CLIPPED,"/original"
                LET crea = os.Path.mkDir(dir2)
                LET dir2 = directorio CLIPPED,"/firmado"
                LET crea = os.Path.mkDir(dir2)
            END IF
        END IF
    END IF
    
RETURN directorio
END FUNCTION

FUNCTION fel_create_docXML(Etiqueta, Valor)
DEFINE docA       xml.DomDocument
DEFINE docNodo    xml.DomNode
DEFINE valNodo    STRING
DEFINE Etiqueta   STRING
DEFINE Valor      STRING

        LET valNodo = '<', Etiqueta,'>', Valor CLIPPED,'</', Etiqueta,'>'
        LET docA = xml.DomDocument.CREATE() 
        CALL docA.setFeature("whitespace-in-element-content",FALSE)
        LET docNodo = docA.createNode(valNodo)
        CALL docA.appendDocumentNode(docNodo)
        
RETURN docA
END FUNCTION