SCHEMA modula

GLOBALS

    DEFINE id INTEGER
    DEFINE fac_database STRING
    DEFINE fac_corr SMALLINT
    DEFINE fac_pdf  SMALLINT
    DEFINE intentos SMALLINT
    DEFINE pStatus  INTEGER
    DEFINE fac_cant_det SMALLINT

    DEFINE factura  RECORD LIKE facturafel_e.*
    DEFINE detalle  RECORD LIKE facturafel_ed.*
    DEFINE conexion RECORD LIKE empresas1.*
    DEFINE fel      RECORD LIKE factura_log.*
    
    DEFINE archivo_firmado STRING
    DEFINE directorio      STRING
    DEFINE directorio2     STRING

    DEFINE detcods DYNAMIC ARRAY OF RECORD
               codigo LIKE facturafel_ed.codigo_p
           END RECORD
END GLOBALS