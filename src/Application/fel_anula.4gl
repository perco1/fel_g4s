# fel_anula.4gl

IMPORT XML

SCHEMA modula

GLOBALS "fel_globales.4gl"
GLOBALS "../Library/ws_g4s/ws_g4s_fel.inc"
GLOBALS "../Library/sat/sat_xml_docto.inc"
GLOBALS "../Library/sat/sat_xml_anula.inc"
GLOBALS "../Library/sat/sat_xml_fcam.inc"

FUNCTION fel_anula_docto()
DEFINE docto RECORD LIKE facturafel_e.*

    CALL STARTLOG("ERR_ANDE")

    INITIALIZE ns1GTAnulacionDocumento.* TO NULL 
    
    LET fel.fac_id = id    
    LET fel.request_id = id USING "&&&&&&&&&&"
    LET fel.fecha_envio = CURRENT
    LET fel.correlativo = ultimo_corr_msg()
    LET fel.estatus = 1
    LET fel.interna_tipod  = factura.tipod
    LET fel.interna_serie  = factura.serie
    LET fel.interna_numero = factura.num_doc
    LET fel.flag_error = 0

    INSERT INTO factura_log VALUES (fel.*)

    IF fel.estatus > 0 THEN

        SELECT * INTO docto.*
          FROM facturafel_e
         WHERE fac_id = factura.id_factura
         
        LET ns1GTAnulacionDocumento.Version = 0.1
        LET ns1GTAnulacionDocumento.SAT.AnulacionDTE.ID = 'DatosCertificados'
        LET ns1GTAnulacionDocumento.SAT.AnulacionDTE.DatosGenerales.ID = 'DatosAnulacion'
        
        LET ns1GTAnulacionDocumento.SAT.AnulacionDTE.DatosGenerales.IDReceptor                  = utl_nit_singuion(factura.nit_r)
        LET ns1GTAnulacionDocumento.SAT.AnulacionDTE.DatosGenerales.NITEmisor                   = utl_nit_singuion(conexion.nit)
        LET ns1GTAnulacionDocumento.SAT.AnulacionDTE.DatosGenerales.NumeroDocumentoAAnular      = docto.autorizacion CLIPPED
        LET ns1GTAnulacionDocumento.SAT.AnulacionDTE.DatosGenerales.FechaEmisionDocumentoAnular = docto.fecha_em CLIPPED
        LET ns1GTAnulacionDocumento.SAT.AnulacionDTE.DatosGenerales.FechaHoraAnulacion          = factura.fecha_anul CLIPPED
        LET ns1GTAnulacionDocumento.SAT.AnulacionDTE.DatosGenerales.MotivoAnulacion             = 'ANULACION POR ELABORACION ERRONEAMENTE'
                
        CALL fel_arch_original()
    END IF

END FUNCTION

PRIVATE FUNCTION fel_arch_original()
DEFINE writertFEL xml.StaxWriter
DEFINE mensaje    STRING
DEFINE resultado  SMALLINT
DEFINE wsstatus   INTEGER
DEFINE retryAuth  INTEGER
DEFINE retryProxy INTEGER
DEFINE retry      INTEGER
DEFINE comando    STRING
DEFINE docXML     xml.DomDocument
DEFINE root       xml.DomNode
DEFINE docto RECORD LIKE facturafel_e.*

   CALL STARTLOG("ERR_ANDE")


    SELECT * INTO docto.*
      FROM facturafel_e
     WHERE fac_id = factura.id_factura
         
  LET wsstatus = -1
  LET retryAuth = FALSE
  LET retryProxy = FALSE
  LET retry = TRUE
  LET fel.dir_arch_org = directorio2 CLIPPED,'/original/oriA_', fel.request_id CLIPPED,'.xml'
  LET fel.dir_arch_fir = directorio2 CLIPPED,'/original/oriA_', fel.request_id CLIPPED,'.xml'
  LET fel.dir_arch_cer = directorio CLIPPED,'/cerA_', fel.request_id CLIPPED,'.xml'
  
  LET resultado = FALSE
  TRY
      LET writertFEL = xml.StaxWriter.Create()
      CALL writertFEL.setFeature("format-pretty-print",TRUE)
      CALL writertFEL.writeTo(fel.dir_arch_org)
      CALL writertFEL.startDocument("utf-8","1.0",FALSE)
      --CALL xml.Serializer.VariableToStax(ns1GTAnulacionDocumento,writertFEL)
      CALL writertFEL.setPrefix('dte','http://www.sat.gob.gt/dte/fel/0.1.0') 
      CALL writertFEL.setPrefix('ds', 'http://www.w3.org/2000/09/xmldsig#')
      CALL writertFEL.setPrefix('xsi', 'http://www.w3.org/2001/XMLSchema-instance')
      --CALL writertFEL.declareDefaultNamespace('dte')
      CALL writertFEL.startElementNS('GTAnulacionDocumento','http://www.sat.gob.gt/dte/fel/0.1.0')
          CALL writertFEL.attribute('Version','0.1')
          CALL writertFEL.startElementNS('SAT','http://www.sat.gob.gt/dte/fel/0.1.0')
              CALL writertFEL.startElementNS('AnulacionDTE','http://www.sat.gob.gt/dte/fel/0.1.0')
                CALL writertFEL.attribute('ID','DatosCertificados')
                CALL writertFEL.emptyElementNS('DatosGenerales','http://www.sat.gob.gt/dte/fel/0.1.0')
                    CALL writertFEL.attribute('ID','DatosAnulacion')
                    CALL writertFEL.attribute('FechaEmisionDocumentoAnular',docto.fecha_em CLIPPED)
                    CALL writertFEL.attribute('FechaHoraAnulacion',    factura.fecha_anul CLIPPED)
                    CALL writertFEL.attribute('IDReceptor',            utl_nit_singuion(factura.nit_r))
                    CALL writertFEL.attribute('NITEmisor',             utl_nit_singuion(conexion.nit))
                    CALL writertFEL.attribute('NumeroDocumentoAAnular',docto.autorizacion CLIPPED)
                    CALL writertFEL.attribute('MotivoAnulacion',       'ANULACION POR ELABORACION ERRONEAMENTE')
              CALL writertFEL.endElement()
          CALL writertFEL.endElement()
      CALL writertFEL.endElement()
      
      CALL writertFEL.endDocument()
      CALL writertFEL.CLOSE()

      --LET docXML = xml.DomDocument.CREATE()
     -- CALL docXML.load(fel.dir_arch_org)
      --LET root = docXML.getFirstDocumentNode()
      --CALL docXML.declareNamespace(root,'ds', 'http://www.w3.org/2000/09/xmldsig#')
      --CALL docXML.declareNamespace(root,'xsi', 'http://www.w3.org/2001/XMLSchema-instance')
      --CALL docXML.save(fel.dir_arch_org)
      --
      --LET comando = 'java -jar FirmaDocumentos-1.0.jar ', 
                    --fel.dir_arch_org CLIPPED,' ', 
                    --conexion.llave_archivo CLIPPED,' ',
                    --conexion.llave_pass CLIPPED,' ', 
                    --fel.dir_arch_fir CLIPPED,
                    --' DatosGenerales http://www.sat.gob.gt/dte/fel/0.1.0'
--
      --RUN comando

      LET resultado = TRUE
      LET docXML = xml.DomDocument.CREATE()
      CALL docXML.load(fel.dir_arch_fir)
      LET fel.xml_docto = docXML.saveToString()
      LET fel.estatus = 2
  CATCH
    LET mensaje = "FEL 2.0 (Archivo) ERROR :",STATUS||" ("||SQLCA.SQLERRM||")"
    LET resultado = FALSE
    LET fel.flag_error = 1
    LET fel.msg_error = mensaje
  END TRY
  
END FUNCTION
