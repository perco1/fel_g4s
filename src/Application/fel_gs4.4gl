IMPORT FGL WSHelper
IMPORT com
IMPORT XML
IMPORT util

SCHEMA modula

GLOBALS "fel_globales.4gl"
GLOBALS "../Library/ws_g4s/ws_g4s_fel.inc"
GLOBALS "../Library/sat/sat_xml_docto.inc"
GLOBALS "../Library/sat/sat_xml_anula.inc"
GLOBALS "../Library/sat/sat_xml_fcam.inc"


FUNCTION fel_gs4_send()
DEFINE mensaje STRING
DEFINE ver_error CHAR(320)
DEFINE error_txt STRING

    CALL STARTLOG("ERR_ANDE") 

    LET FactWSFront_FactWSFrontSoap12Endpoint.Address.Uri = conexion.url_services
    LET ns1RequestTransaction.Requestor   = conexion.requestor CLIPPED 
    LET ns1RequestTransaction.TRANSACTION = 'SYSTEM_REQUEST'
    LET ns1RequestTransaction.Country     = "GT"
    LET ns1RequestTransaction.Entity      = conexion.nit CLIPPED 
    LET ns1RequestTransaction.USER        = conexion.requestor CLIPPED 
    LET ns1RequestTransaction.UserName    = conexion.userface CLIPPED 
    LET ns1RequestTransaction.Data1       = 'POST_DOCUMENT_EMISOR'
    LET ns1RequestTransaction.Data2       = util.Strings.base64Encode( fel.dir_arch_fir )
    LET ns1RequestTransaction.Data3       = fel.request_id

     LET intentos = 1
     LET fel.fecha_envio = CURRENT
     WHILE intentos < 3
        --CALL ui.Interface.refresh()
        
        INITIALIZE wsError.* TO NULL
        LET fel.estatus = 7 -- Eviando el documento por SOAP
        CALL RequestTransaction_g() RETURNING pStatus
        IF pStatus <> -15553 THEN
           LET fel.tipo_respuesta = ns1RequestTransactionResponse.RequestTransactionResult.Response.Result
           IF fel.tipo_respuesta = 1 THEN
                TRY
                    LET fel.estatus = 8 -- RESPUESTA POSITIVA INICAINDO RECUPERAR DATOS
                    LET fel.fecha_finaliza = CURRENT
                    CALL util.Strings.base64Decode( ns1RequestTransactionResponse.RequestTransactionResult.ResponseData.ResponseData1, fel.dir_arch_cer )
                CATCH
                    LET mensaje = "FEL 2.0 (Archivo) ERROR :",STATUS||" ("||SQLCA.SQLERRM||")"
                    LET fel.flag_error = 1
                    LET fel.msg_error = mensaje
                END TRY 
                CALL LlenarDatos()
           ELSE
               LET fel.flag_error = 1
               LET fel.msg_error = ns1RequestTransactionResponse.RequestTransactionResult.Response.Description
               LET ver_error = fel.msg_error
               LET error_txt = ns1RequestTransactionResponse.RequestTransactionResult.Response.Description
               LET fel.fecha_finaliza = CURRENT
               IF ver_error[152,165] = 'PK_MASTERINDEX' THEN
                   LET fel.estatus = 11 -- Ya se encuentra registrado el documeto
                   CALL traer_factura()
               END IF
           END IF
           EXIT WHILE
        ELSE
          LET ns1RequestTransactionResponse.RequestTransactionResult.Response.Result = 0
          LET fel.fecha_finaliza = CURRENT
        END IF
        LET intentos = intentos + 1
        LET mensaje = 'Se realizará un ', intentos USING "#",' ya que expiro el tiempo de espera.'
        DISPLAY mensaje
     END WHILE

     LET fel.intentos = intentos
     
END FUNCTION


FUNCTION fel_gs4_anula()
DEFINE mensaje STRING

    CALL STARTLOG("ERR_ANDE") 

    LET FactWSFront_FactWSFrontSoap12Endpoint.Address.Uri = conexion.url_services
    LET ns1RequestTransaction.Requestor   = conexion.requestor CLIPPED 
    LET ns1RequestTransaction.TRANSACTION = 'SYSTEM_REQUEST'
    LET ns1RequestTransaction.Country     = "GT"
    LET ns1RequestTransaction.Entity      = conexion.nit CLIPPED 
    LET ns1RequestTransaction.USER        = conexion.requestor CLIPPED 
    LET ns1RequestTransaction.UserName    = conexion.userface CLIPPED 
    LET ns1RequestTransaction.Data1       = 'VOID_DOCUMENT'
    LET ns1RequestTransaction.Data2       = util.Strings.base64Encode( fel.dir_arch_fir )
    LET ns1RequestTransaction.Data3       = fel.request_id

     LET intentos = 1
     WHILE intentos < 3
        --CALL ui.Interface.refresh()
        
        INITIALIZE wsError.* TO NULL
        CALL RequestTransaction_g() RETURNING pStatus
        IF pStatus <> -15553 THEN
           LET fel.tipo_respuesta = ns1RequestTransactionResponse.RequestTransactionResult.Response.Result
           IF fel.tipo_respuesta = 1 THEN
                CALL util.Strings.base64Decode( ns1RequestTransactionResponse.RequestTransactionResult.ResponseData.ResponseData1, fel.dir_arch_cer )
                CALL LlenarDatos()
           ELSE
               LET fel.flag_error = 1
               LET fel.msg_error = ns1RequestTransactionResponse.RequestTransactionResult.Response.Description
           END IF
           EXIT WHILE
        ELSE
          LET ns1RequestTransactionResponse.RequestTransactionResult.Response.Result = 0
        END IF
        LET intentos = intentos + 1
        LET mensaje = 'Se realizará un ', intentos USING "#",' ya que expiro el tiempo de espera.'
        DISPLAY mensaje
     END WHILE

     LET fel.intentos = intentos
     
END FUNCTION


FUNCTION traer_factura()
DEFINE id_interno CHAR(30)
DEFINE mensaje SMALLINT 


    CALL STARTLOG("ERR_ANDE")

    DISPLAY id_interno

    LET FactWSFront_FactWSFrontSoap12Endpoint.Address.Uri = conexion.url_services
    LET ns1RequestTransaction.Requestor   = conexion.requestor CLIPPED 
    LET ns1RequestTransaction.Country     = "GT"
    LET ns1RequestTransaction.Entity      = conexion.nit CLIPPED 
    LET ns1RequestTransaction.USER        = conexion.requestor CLIPPED 
    LET ns1RequestTransaction.UserName    = conexion.userface CLIPPED 
    LET ns1RequestTransaction.TRANSACTION = 'LOOKUP_ISSUED_INTERNAL_ID'
    LET ns1RequestTransaction.Data1       = fel.request_id
    LET ns1RequestTransaction.Data2 = '""'
    LET ns1RequestTransaction.Data3 = "XML"

    CALL RequestTransaction_g() RETURNING pStatus
    IF pStatus <> -15553 THEN
       LET fel.tipo_respuesta = ns1RequestTransactionResponse.RequestTransactionResult.Response.Result
       IF fel.tipo_respuesta = 1 THEN
            LET fel.flag_error = 0
            LET fel.msg_error = null
            LET fel.sat_uuid =  ns1RequestTransactionResponse.RequestTransactionResult.Response.Identifier.DocumentGUID CLIPPED
            LET ns1RequestTransaction.TRANSACTION = 'GET_DOCUMENT'
            LET ns1RequestTransaction.Data1       = fel.sat_uuid CLIPPED
            CALL RequestTransaction_g() RETURNING pStatus
            IF pStatus <> -15553 THEN
                LET fel.tipo_respuesta = ns1RequestTransactionResponse.RequestTransactionResult.Response.Result
                IF fel.tipo_respuesta = 1 THEN
                    CALL util.Strings.base64Decode( ns1RequestTransactionResponse.RequestTransactionResult.ResponseData.ResponseData1, fel.dir_arch_cer )
                    CALL LlenarDatos()
                ELSE
                   LET fel.flag_error = 1
                   LET fel.msg_error = ns1RequestTransactionResponse.RequestTransactionResult.Response.Description
                END IF
            END IF
       ELSE
           LET fel.flag_error = 1
           LET fel.msg_error = ns1RequestTransactionResponse.RequestTransactionResult.Response.Description
       END IF
    ELSE
       LET mensaje = "FEL 2.0 (Archivo) ERROR : (-15553) Venció el tiempo de espera. "
       LET fel.flag_error = 1
       LET fel.msg_error = mensaje   
    END IF
END FUNCTION

FUNCTION LlenarDatos()
DEFINE docXML   xml.DomDocument
DEFINE root     xml.DomNode
DEFINE mensaje  STRING

    TRY
        LET docXML = xml.DomDocument.CREATE()
        CALL docXML.load(fel.dir_arch_cer)
        LET fel.xml_respuesta = docXML.saveToString()
        LET root = docXML.getDocumentElement()

        LET fel.estatus = 9 -- Se levantó el XML recibido de G-FACE
        
        CALL fel_dte_datos(root)

        LET fel.estatus = 10 -- Se extrajo información del XML
    CATCH
        LET mensaje = "FEL 2.0 (Archivo) ERROR :",STATUS||" ("||SQLCA.SQLERRM||")"
        LET fel.flag_error = 1
        LET fel.msg_error = mensaje        
    END TRY

END FUNCTION

PRIVATE FUNCTION fel_dte_datos(n)
DEFINE n  xml.DomNode
DEFINE child xml.DomNode
DEFINE nombre CHAR(25)
DEFINE ind SMALLINT
DEFINE node xml.DomNode
DEFINE r xml.DomNode

  IF n IS NOT NULL THEN
    IF n.hasChildNodes() THEN
      LET nombre = n.getLocalName()
      IF nombre = 'NITCertificador' THEN
         LET r = n.getChildNodeItem(1)
         LET fel.certificador_nit = r.getNodeValue()
      END IF 
      IF nombre = 'NombreCertificador' THEN
         LET r = n.getChildNodeItem(1)
         LET fel.certificador_nombre = r.getNodeValue()
      END IF 
      IF nombre = 'FechaHoraCertificacion' THEN
         LET r = n.getChildNodeItem(1)
         LET fel.certificador_fecha = r.getNodeValue()
      END IF 
      LET child = n.getFirstChild()
      WHILE (child IS NOT NULL )
        CALL fel_dte_datos(child)
        LET child = child.getNextSibling()
      END WHILE
    END IF
    IF nombre = 'NumeroAutorizacion' THEN
        LET r = n.getChildNodeItem(1)
        LET fel.sat_uuid = r.getNodeValue()
        IF n.hasAttributes() THEN
          FOR ind = 1 TO n.getAttributesCount()
            LET node = n.getAttributeNodeItem(ind)
            LET fel.certificador_serie = n.getAttribute("Serie")
            LET fel.certificador_numero = n.getAttribute("Numero")
            CALL fel_dte_datos(node)
          END FOR
        END IF
     END IF 
  END IF
END FUNCTION