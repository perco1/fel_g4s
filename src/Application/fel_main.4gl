# fel_main.4gl

IMPORT XML

SCHEMA modula

GLOBALS "fel_globales.4gl"
GLOBALS "../Library/ws_g4s/ws_g4s_fel.inc"
GLOBALS "../Library/sat/sat_xml_docto.inc"
GLOBALS "../Library/sat/sat_xml_anula.inc"

MAIN

    CALL STARTLOG("ERR_ANDE")

    LET id           = arg_val(1)
    LET fac_database = arg_val(2)
    LET fac_corr     = arg_val(3)
    LET fac_pdf      = arg_val(4)

    --LET id = 339 

    IF fac_database IS NOT NULL THEN
        --CONNECT TO fac_database AS 'informix' USING 'losfitos'
        --CONNECT TO fac_database AS 'informix' USING 'informix'
        --Cambio por Carlos CONNECT TO fac_database AS 'informix' USING 'informix'
              CONNECT TO fac_database AS 'informix' USING 'informix'
        --DB MQ PRUEBAS: CONNECT TO fac_database AS 'sistemas' USING 'temporal'
        DISPLAY "Conectado a BD ", fac_database
       --CONNECT TO 'modula@desa_tcp' AS 'informix' USING 'informix' 
       --CONNECT TO fac_database AS 'informix' USING 'informix' 
        IF id > 0 THEN
            INITIALIZE factura.* TO NULL
            SELECT * INTO factura.*
              FROM facturafel_e
             WHERE fac_id = id
               AND estatus = 'P'

            IF factura.fac_id IS NOT NULL THEN

               INITIALIZE conexion.* TO NULL
               SELECT * INTO conexion.*
                 FROM empresas1
                 WHERE bodega = 1

               IF SQLCA.sqlcode <> NOTFOUND THEN
                    LET directorio = fel_valida_directorio(factura.fecha)
                    LET directorio2 = fel_valida_directorio2(factura.fecha)
                    IF (TODAY - factura.fecha) <= 4 THEN
                        LET fac_cant_det = 0
                        IF factura.estado_doc CLIPPED = 'ORIGINAL'  OR
                           factura.estado_doc CLIPPED = 'ANTIGUO' THEN
                            SELECT COUNT(*) INTO fac_cant_det
                              FROM facturafel_ed
                             WHERE fac_id = id
                             
                           IF fac_cant_det > 0 THEN
                               CALL fel_docto_build()
                               --DISPLAY "Despues del build_doc"
                               CALL main_envio_datos('R')
                               --DISPLAY "despues del envio"
                           ELSE
                               DISPLAY "No se encontró detalle para la factura solicitada."
                           END IF
                       ELSE
                           IF factura.estado_doc CLIPPED = 'ANULADO' THEN
                               CALL fel_anula_docto()
                               CALL main_envio_datos('A')
                           ELSE
                               DISPLAY "No se reconoce el estado del documento."
                           END IF
                       END IF
                   ELSE
                       CALL main_crea_log()
                       LET fel.dir_arch_cer = directorio CLIPPED,'/cer_', fel.request_id CLIPPED,'.xml'
                       CALL main_envio_datos('B')
                   END IF
               ELSE
                    CALL main_crea_log()
                    LET fel.flag_error = 1
                    LET fel.msg_error = "FEL 2.0 (Archivo) ERROR : No se encontró datos de conexión al GFACE."
                    
                    UPDATE factura_log SET factura_log.* = fel.*
                    WHERE @correlativo = fel.correlativo
               END IF
            ELSE
                DISPLAY "NO se encontró el número de factura solicitado ", id, " o no tiene estatus 'P'"
            END IF
        ELSE
            DISPLAY "NO es un número de documento válido."
        END IF
    ELSE
        DISPLAY "No se definió la base de datos a usar."
    END IF
    
END MAIN

FUNCTION main_envio_datos(OPCION)
DEFINE opcion CHAR(1)
DEFINE mensaje    STRING
DEFINE resultado  SMALLINT

    IF fel.flag_error = 0 THEN
        CASE opcion
            WHEN 'R' -- REGISTRA
                CALL fel_gs4_send()
            WHEN 'A' -- REGISTRA
                CALL fel_gs4_anula()
            WHEN 'B' -- BUSCA FACTURA
                CALL traer_factura()
        END CASE
        IF fel.flag_error = 0 THEN
            LET fel.cod_archivo_cer = fel_archivo_xml(fel.dir_arch_cer)
        END IF
    END IF

    TRY
        IF fel.flag_error = 0 THEN
            UPDATE facturafel_e
               SET serie_e      = fel.certificador_serie, 
                   numdoc_e     = fel.certificador_numero, 
                   autorizacion = fel.sat_uuid,
                   fel_msg = fel.correlativo,
                   estatus = 'C'
               WHERE @fac_id = id
            DISPLAY "Proceso terminado satisfactoriamente."
        ELSE
            UPDATE facturafel_e
               SET fel_msg = fel.correlativo
             WHERE @fac_id = id
               AND numdoc_e = "0"
            DISPLAY "Proceso terminado con errores."
        END IF
    CATCH
        LET mensaje = "FEL 2.0 (Archivo) ERROR :",STATUS||" ("||SQLCA.SQLERRM||")"
        LET resultado = FALSE
        LET fel.flag_error = 1
        LET fel.msg_error = mensaje
    END TRY
    
    UPDATE factura_log SET factura_log.* = fel.*
    WHERE @correlativo = fel.correlativo
END FUNCTION


FUNCTION main_crea_log()
DEFINE numero_acceso  DECIMAL(9,0)

    CALL STARTLOG("ERR_ANDE")

    INITIALIZE ns1GTDocumento.* TO NULL 

    LET fel.fac_id = id    
    LET fel.request_id = id USING "&&&&&&&&&&"
    LET fel.fecha_envio = CURRENT
    LET fel.correlativo = ultimo_corr_msg()
    LET fel.numero_acceso = numero_acceso
    LET fel.estatus = 1 -- ESTADO INICIAL
    LET fel.interna_tipod  = factura.tipod
    LET fel.interna_serie  = factura.serie
    LET fel.interna_numero = factura.num_doc
    LET fel.flag_error = 0

    INSERT INTO factura_log VALUES (fel.*)

END FUNCTION
{
--LET fel.estatus = 1 -- ESTADO INICIAL
--LET fel.estatus = 2 --TERMINÓ DE GRABAR INFORMACIÓN BÁSICA
--LET fel.estatus = 3 --TERMINÓ DE GRABAR ADENDAS Y COMPLEMENTOS
--LET fel.estatus = 4 --GENERÓ EL XML EN ARCHIVO ORIGINAL
--LET fel.estatus = 5 --GENERÓ EL XML FIRMADO
--LET fel.estatus = 6 -- LEVANTÓ EL ARCHIVO FIRMADO A MEMORIA
--LET fel.estatus = 7 -- Eviando el documento por SOAP
--LET fel.estatus = 8 -- RESPUESTA POSITIVA INICAINDO RECUPERAR DATOS
--LET fel.estatus = 9 -- Se levantó el XML recibido de G-FACE
--LET fel.estatus = 10 -- Se extrajo información del XML
--LET fel.estatus = 11 -- Ya se encuentra registrado el documeto
}

