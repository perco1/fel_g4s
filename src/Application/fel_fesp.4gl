IMPORT XML

SCHEMA modula

GLOBALS "fel_globales.4gl"
GLOBALS "../Library/ws_g4s/ws_g4s_fel.inc"
GLOBALS "../Library/sat/sat_xml_docto.inc"
GLOBALS "../Library/sat/sat_xml_anula.inc"
GLOBALS "../Library/sat/sat_xml_fcam.inc"


FUNCTION fel_adenada_FESP()
DEFINE IdInterno  STRING
DEFINE numero      STRING
DEFINE i, j, a     SMALLINT
DEFINE valor      STRING

    CALL STARTLOG("ERR_ANDE")
    
    LET numero = factura.num_doc
    LET IdInterno = 'Ref. ', factura.tipod CLIPPED, ' ', factura.serie CLIPPED, '-', numero.trimRight()
    LET j = detcods.getLength()

    LET a = 1
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('IdInterno',IdInterno)
    LET a = a + 1
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('TotalEnLetras',factura.total_en_letras)
    LET a = a + 1
    LET valor = factura.tasa1
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('TasaDeCambio', valor.trimRight())
    LET a = a + 1
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('Observaciones','000')
    LET a = a + 1
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('RetencionISR','0')
    LET a = a + 1
    LET ns1GTDocumento.SAT.Adenda._LIST_0[a] = fel_create_docXML('RetencionIVA','0')

END FUNCTION

FUNCTION fel_complemento_FESP()
DEFINE wrtEspecial  xml.StaxWriter
DEFINE docEspecial  xml.DomDocument
DEFINE tRetenciones DECIMAL(10,2)
DEFINE rISR DECIMAL(10,2)
DEFINE archivo   STRING

    CALL STARTLOG("ERR_ANDE")

    LET archivo = fel_archivo_genera()

    LET rISR = factura.base1 * 0.05

    LET tRetenciones = factura.total_neto - (factura.monto1 + rISR)
    LET wrtEspecial = xml.StaxWriter.Create()
    CALL wrtEspecial.setFeature("format-pretty-print",TRUE)
    CALL wrtEspecial.writeTo(archivo)
    CALL wrtEspecial.startDocument("utf-8","1.0",FALSE)

    CALL wrtEspecial.setPrefix('cfe','http://www.sat.gob.gt/face2/ComplementoFacturaEspecial/0.1.0')
    CALL wrtEspecial.startElementNS("RetencionesFacturaEspecial",'http://www.sat.gob.gt/face2/ComplementoFacturaEspecial/0.1.0')
    CALL wrtEspecial.ATTRIBUTE('Version','1')
    CALL wrtEspecial.declareNamespace('cfe','http://www.sat.gob.gt/face2/ComplementoFacturaEspecial/0.1.0')
    CALL wrtEspecial.declareNamespace('xsi','http://www.w3.org/2001/XMLSchema-instance')
    CALL wrtEspecial.startElementNS("RetencionISR",'http://www.sat.gob.gt/face2/ComplementoFacturaEspecial/0.1.0')
    CALL wrtEspecial.characters(rISR)
    CALL wrtEspecial.endElement()
    CALL wrtEspecial.startElementNS("RetencionIVA",'http://www.sat.gob.gt/face2/ComplementoFacturaEspecial/0.1.0')
    CALL wrtEspecial.characters(factura.monto1)
    CALL wrtEspecial.endElement()
    CALL wrtEspecial.startElementNS("TotalMenosRetenciones",'http://www.sat.gob.gt/face2/ComplementoFacturaEspecial/0.1.0')
    CALL wrtEspecial.characters(tRetenciones CLIPPED)
    CALL wrtEspecial.endElement()
    CALL wrtEspecial.endElement()
    
    CALL wrtEspecial.endDocument()
    CALL wrtEspecial.CLOSE()

    LET docEspecial = xml.DomDocument.Create()
    CALL docEspecial.setFeature("whitespace-in-element-content",FALSE)

    CALL docEspecial.LOAD(archivo)

    CALL fel_archivo_elimina(archivo)

RETURN docEspecial
END FUNCTION